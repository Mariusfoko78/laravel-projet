<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthManager;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\LoginController;



use App\Http\Controllers\CourseController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home');
Route::get('/navigation', [AuthManager::class, 'navigation'])->name('navigation');
// web.php
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/Liste-des-cours', [App\Http\Controllers\CourseController::class, 'course'])->name('course');
Route::get('/profil', [UserController::class, 'profil'])->name('profil');
Route::get('/Liste-des-utilisateurs',[userController::class,'user'])->name('Liste-des utilisateur');
Route::get('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('login');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin/users', 'UserController@listUsers')->name('admin.users.list');
});

Route::get('/export-profile', 'ProfileController@export')->name('export');
Route::get('/profil', [ProfilController::class, 'show'])->name('profil');
Route::get('/login', [LoginController::class, 'login'])->name('login');

// Route pour afficher le formulaire de connexion
Route::get('/login', [LoginController::class, 'login'])->name('login');

// Route pour gérer la soumission du formulaire de connexion
Route::post('/login', [LoginController::class, 'authenticate']);


Route::group(['middleware' => 'admin'], function () {
    // Les routes nécessitant un accès administratif
    Route::get('/admin', 'AdminController@index')->name('admin.index');
    // ...
});





Route::post('/login', [AuthManager::class, 'loginPost'])->name('login.post');
Route::put('/users/{id}', [UserController::class, 'update'])->name('users.update');

Route::get('/registration', [AuthManager::class, 'registration'])->name('registration');
Route::post('/registration', [AuthManager::class, 'registrationPost'])->name('registration.post');
Route::get('/logout', [AuthManager::class, 'logout'])->name('logout');
Route::group(['middleware' => 'auth'], function (){
    Route::get('/profile', function (){
        return "Hi";
    });
});

