<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        // vérifier si l'utilisateur est connecté
        if (Auth::check()) {
            // si l'utilisateur a un rôle d'administrateur
            if (Auth::user()->admin) {
                return $next($request);
            }
        }
        return redirect('/')->with('error','Accès refusé vous devez être administrateur.');
    }
}
