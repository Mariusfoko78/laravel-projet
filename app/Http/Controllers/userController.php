<?php

namespace App\Http\Controllers;
use App\Models\user;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class userController extends Controller
{

    public function store(Request $request)
    {
        // Validation des données de l'utilisateur
        $validatedData = $request->validate([
            'identifiant'
        ]);
    }


    public function edit($id)
    {
        // logique de récup
        $user = user::find($id);
        return view('user.edit', compact('user'));

    }

    public function update(Request $request, $id) {

        $user = User::find($id);
        if (!$user) {
            return redirect()->route('acceuil')->with('erreur', 'L\'utilisateur n\'a pas été trouvé');
        }
        }


    public function profil()
    {
        // Vérifiez si l'utilisateur est connecté
        if (!Auth::check()) {
            return redirect()->route('login')->with('error', 'Vous devez être connecté pour accéder à votre profil.');
        }

        // Récupérez les informations de l'utilisateur connecté
        $user = Auth::user();

        return view('profil', compact('user'));
    }
    public function listUsers()
    {
        $users = User::all(); // Récupérez tous les utilisateurs depuis la base de données

        return view('admin.users.list', ['users' => $users]);
    }

}


