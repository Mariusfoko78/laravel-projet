<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class ProfilController extends Controller
{
    public function show()
    {
        // Utilisez le middleware 'auth' pour s'assurer que seul l'utilisateur authentifié peut accéder à cette page
        $this->middleware('auth');

        // Récupérez l'utilisateur connecté
        $user = auth()->user();

        // Vérifiez si l'utilisateur existe
        if (!$user) {
            return redirect()->route('home')->with('erreur', 'L\'utilisateur n\'a pas été trouvé');
        }

        // Renvoyez les données de l'utilisateur vers la vue du profil
        return view('profil', compact('user'));
    }
}





