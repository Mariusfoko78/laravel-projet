<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


use Illuminate\Support\Facades\Session;



class AuthManager extends Controller
{

    //la méthode pour afficher le formulaire de connexion
   public function login(){
        if (Auth::check()){
            return redirect(route('home'));
        }
        return view('login');
    }

    // parti du code pour l'enregistrement d'un new utilisateur
  public  function registration(){
        if (Auth::check()){
            return redirect(route('home'));
        }
        return view('registration');
    }

// validation des des informations de connexion de l'utilisateur
   public function loginPost(Request $request){
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $credentials = $request->only('username', 'password');
        if(Auth::attempt($credentials)){

            // connexion réussie, redirigez l'utilisateur vers le profil
            return redirect()->route('profil')->with('success','Bienvenue sur votre profil');
        }

        //
        return redirect(route('login'))->with("error", "Les informations de connexion ne sont pas valides");
    }

// validation des données d'inscription, de la création d'un nouvel utilisateur
  public  function registrationPost(Request $request){
        $request->validate([
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role'=>'in:visiteur,étudiant,professeur,administrateur',
        ]);

      // Comptez le nombre d'utilisateurs existants
      $userCount = User::count();

        // creation d'un nouvel utilisateur
        $data['username'] = $request->username;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);

      // Si c'est le premier utilisateur, attribuez-lui le rôle "administrateur"
      if ($userCount === 0) {
          $data['role'] = 'administrateur';
      } else {
          // Sinon, attribuez-lui le rôle par défaut (par exemple, "visiteur")
          $data['role'] = 'visiteur';
      }

      // créé l'utilisateur
        $user = User::create($data);
        if(!$user){
            return redirect(route('registration'))->with("error", "Échec de l’enregistrement, réessayer.");
        }
        return redirect(route('login'))->with("success", "L'utilisateur  a été créé avec succès");

    }



// la déconnexion de l'utilisateur actuellement authentifié.
    public function logout(Request $request)
    {
        Auth::logout(); // Déconnectez l'utilisateur

        $request->session()->invalidate();

        // Redirigez l'utilisateur vers la page d'accueil avec le message "Au revoir"
        return redirect(route('home'))->with('message', 'Au revoir');
    }


    //  la validation des informations de connexion de l'utilisateur
    public function navigation()
    {
        $userRole = Auth::check() ? Auth::user()->role : null;

        return view('navigation', ['userRole' => $userRole]);
    }


    public function show()
    {
        // Utiliser le middleware 'auth' pour s'assurer que seul l'utilisateur authentifié peut accéder à cette page
        $this->middleware('auth');

        // Récupérer l'utilisateur connecté
        $utilisateur = auth()->user();

        // Vérifier si l'utilisateur existe
        if (!$utilisateur) {
            return redirect()->route('home')->with('erreur', 'L\'utilisateur n\'a pas été trouvé');
        }

        // Afficher la vue du profil avec les données de l'utilisateur
        return view('profil', compact('utilisateur'));
    }


}







