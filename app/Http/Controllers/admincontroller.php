<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminController extends Controller
{
    // Méthode pour afficher la liste des utilisateurs avec leurs rôles actuels
    public function List()
    {
        $users = User::all();
        return view('admin.userList', compact('users'));
    }

    // Méthode pour attribuer le rôle Étudiant à un utilisateur
    public function assign(Request $request, $userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return redirect()->back()->with('erreur', 'Utilisateur introuvable');
        }

        // Assigner le rôle Étudiant à l'utilisateur
        $user->role = 'Étudiant';
        $user->save();

        return redirect()->back()->with('message', 'Le rôle Étudiant a été attribué avec succès.');
    }

    // Méthode pour attribuer le rôle Professeur à un utilisateur (à prévoir plus tard)
    public function assignProfessorRole(Request $request, $userId)
    {
        // Assurez-vous que l'utilisateur actuel est autorisé à effectuer cette action (administrateur, par exemple)
        if (!Auth::user() || !Auth::user()->admin) {
            return redirect()->route('home')->with('error', 'Vous n\'êtes pas autorisé à attribuer le rôle de professeur.');
        }

        // Recherchez l'utilisateur en utilisant son ID
        $user = User::find($userId);

        // Vérifiez si l'utilisateur a été trouvé
        if (!$user) {
            return redirect()->route('admin.users')->with('error', 'Utilisateur non trouvé.');
        }

        // Mettez à jour le rôle de l'utilisateur pour le définir en tant que professeur
        $user->role = 'professeur';
        $user->save();

        return redirect()->route('admin.users')->with('success', 'Rôle de professeur attribué avec succès à l\'utilisateur.');
    }


    // Méthode pour afficher la liste des utilisateurs avec leurs rôles actuels (à prévoir plus tard)

    public function updateUserRole(Request $request)
    {
        $user = User::find($request->input('user_id'));
        if ($user) {
            $user->role = $request->input('role');
            $user->save();
        }
        return redirect('/admin/users')->with('success', 'Rôle utilisateur mis à jour avec succès.');
    }


}

