@include('include.header')

@section('content')
    @extends('layout')
    <div class="container">
        <table class="table table-striped">
        <h3>Votre Profil</h3>
        <th>Intitulé</th>
        <th class="">Valeur</th>

            <tr>
                <th>Identifiant :</th>


                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>Création :</th>
                <td>{{ $user->created_at->format('d/m/Y') }}</td>
            </tr>
            <tr>
                <th>Dernière visite :</th>
                <td>{{date_format(new DateTime($user->lastlogin),' d/m/y H:i:s')}}</td>
            </tr>
            <tr>
                <th>Rôle</th>
                @if($user->role == 1)
                    <td>Étudiant</td>
                @elseif($user->role == 2)
                    <td>Professeur</td>
                @elseif($user->role == 3)
                    <td>Administrateur</td>
                @else
                    <td>Visiteur</td>
                @endif
            </tr>
        </table>
        <a href="{{ route('export') }}" class="btn btn-primary">Exporter le profil en JSON</a>
    </div>


@endsection


