
<?php


@include('include.header');
@section('content')
    @extends('layout') <!-- Assurez-vous d'avoir un layout de base -->

    @section('content')
        <div class="container">
            <h1>Liste des utilisateurs</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Identifiant</th>
                    <th>Email</th>
                    <th>Date de création</th>
                    <th>Date et heure de dernière visite</th>
                    <th>Rôle</th>
                    <th>Changer de rôle</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->format('d/m/Y H:i:s') }}</td>
                        <td>{{ $user->last_login_at ? $user->last_login_at->format('d/m/Y H:i:s') : 'Jamais' }}</td>
                        <td>{{ $user->role }}</td>
                        <td>
                            <!-- Formulaire pour changer le rôle -->
                            <form action="{{ route('admin.users.update-role', $user->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <select name="new_role">
                                    <option value="visiteur">Visiteur</option>
                                    <option value="étudiant">Étudiant</option>
                                    <option value="professeur">Professeur</option>
                                </select>
                                <button type="submit" class="btn btn-primary">Changer le rôle</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endsection

